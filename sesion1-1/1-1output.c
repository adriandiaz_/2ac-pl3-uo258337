#include <stdio.h>

int main()
{
    int i = 3;
    float f = 2.4;
    char * s = "Content";

    // Print the content of i
    printf("This is the content of i=%d \n",i);

    // Print the content of f
    printf("This is the content of f=%f \n",f);

    // Print the content of the string
    printf("This is the content of s=%s \n",s);

    return 0;
}
